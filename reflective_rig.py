import nplab
from nplab.instrument.camera.opencv import OpenCVCamera
from nplab.instrument.spectrometer.seabreeze import OceanOpticsSpectrometer
from nplab.instrument.stage.thorlabs_kinesis import BenchtopPiezo
from nplab.utils.gui import QtWidgets, QtCore, QtGui
from nplab.utils.notified_property import NotifiedProperty, DumbNotifiedProperty
from nplab.ui.ui_tools import UiTools, QuickControlBox
from nplab.instrument import Instrument
import nplab.experiment.gui
import nplab.experiment
import time
import numpy as np
import scipy
import matplotlib.pyplot as plt
import threading
from tqdm.auto import tqdm
from scipy import ndimage

class ReflectiveRig(Instrument):
    """A class representing the reflective rig, including a couple of webcams,
    a spectrometer (or two), and a piezo for sample positioning.
    """
    def __init__(self, cameras, spectrometers, piezo=None):
        super(ReflectiveRig, self).__init__()
        # Check we've got the right hardware
        assert isinstance(cameras[0], OpenCVCamera), "Cameras must be OpenCVCameras"
        assert len(cameras) == 2 # TODO: figure out a sensible way to get them the right way round!
        #TODO: better checking (e.g. assert camera has color_image, gray_image methods)
        self.cameras = cameras
        self.spectrometers = spectrometers
        self.piezo = piezo
    
    @NotifiedProperty
    def live_view(self):
        """Whether the camera is currently streaming and displaying video"""
        live = True
        for cam in self.cameras:
            if cam.live_view is False:
                live = False
        return live
    @live_view.setter
    def live_view(self, live_view):
        for cam in self.cameras:
            cam.live_view = live_view
            
    def get_qt_ui(self):
        return ReflectiveRigUI(self)
    
    def smooth_move(self, pos, axis=None, dx=1, dt=0.2):
        """Move the piezo smoothly (should this be built in to the class?)"""
        if axis is None:
            return self.smooth_move_3d(pos, dx=dx, dt=dt)
        else:
            return self.smooth_move_1d(pos, axis, dx=dx, dt=dt)
        
    def smooth_move_1d(self, pos, axis, dx=0.5, dt=0.1):
        """Move the piezo smoothly (should this be built in to the class?)"""
        oldpos = self.piezo.select_axis(self.piezo.position, axis)
        N = int(np.abs(pos-oldpos)//dx)
        moves = []
        for i in range(N, 0, -1):
            newpos = pos - np.float(i * dx * np.sign(pos - oldpos))
            self.piezo.move(newpos, axis=axis, relative=False)
            time.sleep(dt)
        self.piezo.move(pos, axis=axis, relative=False)
        
    def smooth_move_3d(self, pos, dx=1, dt=0.2):
        """Move the piezo smoothly (should this be built in to the class?)"""
        oldpos = np.array(self.piezo.position)
        distance = np.sum((pos-oldpos)**2)**0.5
        N = int(distance//dx)
        for i in range(N, 0, -1):
            newpos = pos - np.float(i/N) * (pos - oldpos)
            self.piezo.move(newpos, relative=False)
            time.sleep(dt)
        self.piezo.move(pos, relative=False)
    
class TimelapseImagesExperiment(nplab.experiment.gui.ExperimentWithProgressBar):
    def __init__(self, rig, N=3, dt=0.5):
        """Set up a timelapse experiment"""
        super(TimelapseImagesExperiment, self).__init__()
        self.rig = rig
        self.N = N
        self.dt = dt
        
    def prepare_to_run(self):
        self.datagroup = nplab.current_datafile().create_group("timelapse_%d")
        self.progress_maximum = self.N
        
    def run(self):
        try:
            for i in range(self.N):
                g = self.datagroup.create_group("timepoint_%d")
                if i > 0:
                    self.wait_or_stop(self.dt)
                for cam in self.rig.cameras:
                    g.create_dataset("image_%d", data=cam.raw_image())
                self.update_progress(i+1)
        except nplab.experiment.ExperimentStopped:
            pass

class LinearScanExperiment(nplab.experiment.gui.ExperimentWithProgressBar):
    def __init__(self, rig, voltages, axis="channel_0", exposures=[None], note=None):
        super(LinearScanExperiment, self).__init__()
        self.rig = rig
        self.voltages = voltages
        self.axis = axis
        self.exposures = exposures
        self.note = note
        
    def prepare_to_run(self):
        self.progress_maximum = len(self.voltages)
        self.datagroup = nplab.current_datafile().create_group("linear_scan_%d")
        self.datagroup.attrs['description'] = ("The PSF is measured by recording "
            "images/spectra at different Z positions.  Currently (6/12), the "
            "Z position is the first voltage for the Piezo.  The "
            "scaling from volts to microns should be 75v to 20um.\n \n"
            "If multiple cameras or spectrometers are present, readings "
            "are taken from all of them - camera_index or spectrometer_index "
            "is added as an attribute to distinguish which one is which.")
        if self.note is not None:
            self.datagroup.attrs['note'] = self.note
        
    def run(self):
        piezo = self.rig.piezo
        axis=self.axis
        cameras = self.rig.cameras
        spectrometers = self.rig.spectrometers
        previous_voltage = piezo.select_axis(piezo.position, self.axis)
        previous_exposures = [c.exposure for c in cameras]
        for ci, cam in enumerate(cameras):
            for exp in self.exposures:
                if exp is not None:
                    cam.exposure = exp
                time.sleep(0.2)
                dumped_image = cam.raw_image()
                ds = self.datagroup.create_dataset("initial_uncropped_image_%d", data=cam.raw_image())
                ds.attrs["piezo_output_voltages"] = piezo.output_voltages
                ds.attrs["camera_index"] = ci
                ds.attrs["exposure"] = cam.exposure
        try:
            for i, voltage in enumerate(self.voltages):
                target_voltage = previous_voltage + voltage
                self.rig.smooth_move(previous_voltage + voltage, axis=self.axis)
                time.sleep(0.2)
                piezo.move(target_voltage, axis=axis, relative=False)
                time.sleep(0.5)
                vg = self.datagroup.create_group("position_%d", 
                                            attrs={"piezo_output_voltages":piezo.output_voltages})
                for ci, cam in enumerate(cameras):
                    for e in self.exposures:
                        if e is not None:
                            cam.exposure = e
                        time.sleep(0.2)
                        dumped_image = cam.raw_image()
                        w, h = dumped_image.shape[:2]
                        ds = vg.create_dataset("camera_%d", data=cam.raw_image()[w*2//5:w*3//5, h*2//5:h*3//5, ...])
                        ds.attrs["camera_index"] = ci
                        ds.attrs["piezo_output_voltages"] = piezo.output_voltages
                        ds.attrs["exposure"] = cam.exposure
                for j in range(5):
                    for si, spectrometer in enumerate(spectrometers):
                        ds = vg.create_dataset("spectrum_%d", data=spectrometer.read_spectrum(), attrs=spectrometer.metadata)
                        ds.attrs["piezo_output_voltages"] = piezo.output_voltages
                        ds.attrs["spectrometer_index"] = si
                self.update_progress(i+1)
            for i in range(len(self.rig.spectrometers)):
                self.extract_spectra(spectrometer=i)
        except nplab.experiment.ExperimentStopped:
            pass
        finally:
            self.rig.smooth_move(previous_voltage, axis=axis)
            for camera, exposure in zip(cameras, previous_exposures):
                camera.exposure = exposure
                
    def extract_spectra(self, group=None, save_stack=True, spectrometer=0, use_reference=True):
        """Average together spectra and create a 2D stack vs position, wavelength
        """
        group = self.datagroup if group is None else group
        positions = group.numbered_items("position")
        for i, p in enumerate(positions):
            spectra = [s for s in p.numbered_items("spectrum") if s.attrs['spectrometer_index']==spectrometer]
            if i==0:
                stack = np.zeros((len(positions), len(spectra)) + spectra[0].shape, dtype=spectra[0].dtype)
                wavelengths = spectra[0].attrs['wavelengths']
                try:
                    background = spectra[0].attrs['background']
                    reference = spectra[0].attrs['reference']
                except:
                    print("warning: no background/reference saved")
                voltages = np.zeros((len(positions), 3))
            for j, s in enumerate(spectra):
                stack[i, j, ...] = s # should work for colour or single-channel
            voltages[i, :] = spectra[0].attrs['piezo_output_voltages']
        try:
            pass
            stack -= background[np.newaxis, np.newaxis, ...]
            if use_reference:
                stack /= (reference-background)[np.newaxis, np.newaxis, ...]
        except:
            print("Could not reference/background-subtract the spectra")
        if save_stack:
            ds = group.create_dataset("stacked_spectra_%d", data=stack)
            ds.attrs['wavelengths'] = wavelengths
            ds.attrs['voltages'] = voltages
        return wavelengths, voltages, stack
    
    def extract_images(self, group=None, camera=0, exposure=0):
        group = self.datagroup if group is None else group
        positions = group.numbered_items("position")
        voltages = np.zeros((len(positions), 3))
        for i, p in enumerate(positions):
            image = p.numbered_items("camera")[camera]
            if i==0:
                stack = np.zeros((len(positions), ) + image.shape, dtype=image.dtype)
            stack[i, ...] = image # should work for colour or single-channel
            voltages[i, :] = image.attrs['piezo_output_voltages']
        return voltages, stack
        
class ReflectiveRigUI(QtWidgets.QWidget):
    """Generic user interface for a camera."""
    def __init__(self, rig):
        super(ReflectiveRigUI, self).__init__()
        self.rig = rig
        
        # Set up the UI        
        self.setWindowTitle("Reflective rig camera view")
        self.splitter = QtWidgets.QSplitter()
        self.splitter.setOrientation(0) # Make it vertical
        # The image display goes at the top of the window
        self.cameras_layout = QtWidgets.QHBoxLayout()
        self.preview_widgets = []
        self.parameters_widgets = []
        for cam in self.rig.cameras:
            camera_layout = QtWidgets.QVBoxLayout() # Lay out each camera vertically
            self.preview_widgets.append(cam.get_preview_widget())
            camera_layout.addWidget(self.preview_widgets[-1]) # Preview at the top
            cb = QuickControlBox("Controls")
            cb.add_spinbox("exposure") # Exposure control underneath
            cb.auto_connect_by_name(cam)
            camera_layout.addWidget(cb) # Exposure control underneath
            self.cameras_layout.addLayout(camera_layout)
        self.cameras_layout_widget = QtWidgets.QWidget()
        self.cameras_layout_widget.setLayout(self.cameras_layout)
        self.splitter.addWidget(self.cameras_layout_widget)
        
        hl = QtWidgets.QHBoxLayout()
        self.quick_controls = QuickControlBox("Quick controls")
        self.quick_controls.add_checkbox("live_view")
        self.quick_controls.add_button("save_images_and_spectra", "Save images and spectra")
        self.quick_controls.add_lineedit("note")
        self.quick_controls.add_button("add_note")
        self.quick_controls.auto_connect_by_name(self)
        self.quick_controls.auto_connect_by_name(self.rig)
        hl.addWidget(self.quick_controls)
        
        self.tabs = QtWidgets.QTabWidget()
        self.tab_contents = []
        
        cb = QuickControlBox("Timelapse")
        cb.add_spinbox("timelapse_n_images")
        cb.add_button("take_timelapse", "Start")
        cb.auto_connect_by_name(self)
        self.tabs.addTab(cb, "Timelapse")
        self.tab_contents.append(cb)
        
        cb = QuickControlBox("Linear Scan")
        cb.add_spinbox("start_voltage")
        cb.add_spinbox("stop_voltage")
        cb.add_checkbox("sweep_exposure")
        cb.add_spinbox("start_exposure")
        cb.add_spinbox("stop_exposure")
        cb.add_combobox("scan_axis", self.rig.piezo.axis_names)
        cb.add_button("linear_scan", "Start")
        cb.auto_connect_by_name(self)
        self.linear_scan_control_box = cb
        self.tabs.addTab(cb, "Linear Scan")
        self.tab_contents.append(cb)
        
        hl.addWidget(self.tabs)
        hl_widget = QtWidgets.QWidget()
        hl_widget.setLayout(hl)
        self.splitter.addWidget(hl_widget)
        
        # Next: a VU meter for total spectrometer power?
        #layout.setContentsMargins(5,5,5,5)
        #layout.setSpacing(5)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.splitter)
        self.setLayout(layout)
        
    timelapse_n_images = DumbNotifiedProperty(4)
    
    #@NotifiedProperty
    #def live_view(self):
    #    """Whether the camera is currently streaming and displaying video"""
    #    return self.rig.live_view
    #@live_view.setter
    #def live_view(self, live_view):
    #   self.rig.live_view = live_view
        
    note = DumbNotifiedProperty("")
    
    def add_note(self):
        nplab.current_datafile().create_dataset("note_%d", data=self.note)
        
    def save_images_and_spectra(self):
        group = nplab.current_datafile().create_group("snapshot_%d")
        group.attrs['description'] = "An image from each camera, and a spectrum from each spectrometer."
        group.attrs['note'] = self.note
        for cam in self.rig.cameras:
            group.create_dataset("image_%d", data=cam.raw_image())
        for spectrometer in self.rig.spectrometers:
            group.create_dataset("spectrum_%d", data=spectrometer.read_spectrum(), attrs=spectrometer.metadata)
        
    start_voltage = DumbNotifiedProperty(-5)
    stop_voltage = DumbNotifiedProperty(6)
    start_exposure = DumbNotifiedProperty(-4)
    stop_exposure = DumbNotifiedProperty(0)
    sweep_exposure = DumbNotifiedProperty(True)
    scan_axis = DumbNotifiedProperty(0)
        
    def take_timelapse(self):
        self.rig._running_experiment = TimelapseImagesExperiment(self.rig, N=self.timelapse_n_images)
        self.rig._running_experiment.run_modally()
        
    def linear_scan(self):
        print("starting linear scan")
        e = LinearScanExperiment(self.rig, 
                                 np.arange(self.start_voltage, self.stop_voltage),
                                 axis=self.rig.piezo.axis_names[self.scan_axis],
                                 exposures=np.arange(self.start_exposure, self.stop_exposure) if self.sweep_exposure else [None],
                                 note=self.note)
        print("created experiment")
        self.rig._running_experiment = e
        print("running experiment")
        e.run_modally()
        print("done")

    
    
    