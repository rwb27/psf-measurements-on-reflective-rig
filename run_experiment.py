# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 15:02:28 2019

@author: Hera
"""

import nplab
from nplab.instrument.camera.opencv import OpenCVCamera
from nplab.instrument.spectrometer.seabreeze import OceanOpticsSpectrometer
from nplab.instrument.spectrometer import Spectrometers
from nplab.instrument.stage.thorlabs_kinesis import BenchtopPiezo, list_devices
from nplab.utils.gui import QtWidgets, QtCore, QtGui
from nplab.utils.notified_property import NotifiedProperty, DumbNotifiedProperty
from nplab.ui.ui_tools import UiTools, QuickControlBox
from nplab.instrument import Instrument
import nplab.experiment.gui
import nplab.experiment
import time
import numpy as np
import scipy
import matplotlib.pyplot as plt
import threading
from tqdm.auto import tqdm
from scipy import ndimage

import reflective_rig
from importlib import reload

#%%

%gui qt
%matplotlib inline

#%%

df = nplab.current_datafile()
cameras = [OpenCVCamera(i) for i in range(2)]
spectrometers = OceanOpticsSpectrometer.get_spectrometers()
for s in spectrometers.spectrometers:
    s.set_tec_temperature(-5)
piezo = BenchtopPiezo('71809230')

#%%
reload(reflective_rig)
rig = reflective_rig.ReflectiveRig(cameras, spectrometers.spectrometers, piezo)
ui = rig.show_gui(blocking=False)
for w in cameras[0]._preview_widgets:
    w.set_crosshair_centre((236,292))

#%%
df.show_gui(blocking=False)
spectrometers.show_gui(blocking=False)
#spectrometers_composite = Spectrometers(spectrometers)
#spectrometers_composite.show_gui(blocking=False)

#%%

last_clicked=(236, 292)
def remember_point(x, y):
    global last_clicked
    last_clicked = x, y
cameras[0].set_legacy_click_callback(remember_point)

#%%
for w in cameras[0]._preview_widgets:
    w.set_crosshair_centre(last_clicked)

#%%

nplab.close_current_datafile()
for c in cameras:
    c.live_view=False
    c.close()
    
piezo.close()
